package com.enliktjioe.popularmovie.data;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.Uri;
import android.preference.PreferenceManager;

import com.enliktjioe.popularmovie.R;

public final class SortUtil {

    private SortUtil() {

    }

    public static SortCommon getSortByPreference(Context context) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        String sort = prefs.getString(
                context.getString(R.string.key_sort_by),
                context.getString(R.string.key_sort_popularity)
        );
        return SortCommon.fromString(sort);
    }

    public static Uri getSortedMoviesUri(Context context) {
        SortCommon sortCommon = getSortByPreference(context);
        switch (sortCommon) {
            case MOST_POPULAR:
                return MoviesConnect.MostPopularMovies.CONTENT_URI;
            case MOST_RATED:
                return MoviesConnect.MostRatedMovies.CONTENT_URI;
            default:
                throw new IllegalStateException("Unknown sortCommon.");
        }
    }

    public static void saveSortByPreference(Context context, SortCommon sortCommon) {
        SharedPreferences.Editor editor = PreferenceManager.getDefaultSharedPreferences(context).edit();
        editor.putString(
                context.getString(R.string.key_sort_by),
                sortCommon.toString()
        );
        editor.apply();
    }
}
