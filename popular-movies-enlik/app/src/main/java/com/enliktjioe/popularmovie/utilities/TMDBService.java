package com.enliktjioe.popularmovie.utilities;

import com.enliktjioe.popularmovie.data.MoviePreferences;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface TMDBService {

    @GET("movie/{sort_by}")
    Call<ResponseUtils<MoviePreferences>> discoverMovies(@Path("sort_by") String sortBy, @Query("page") Integer page);

}
