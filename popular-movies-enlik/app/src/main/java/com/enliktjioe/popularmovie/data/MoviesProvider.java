package com.enliktjioe.popularmovie.data;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.support.annotation.NonNull;

import java.util.Arrays;
import java.util.HashSet;

public class MoviesProvider extends ContentProvider {

    static final int MOVIES = 100;
    static final int MOVIE_BY_ID = 101;
    static final int MOST_POPULAR_MOVIES = 201;
    static final int MOST_RATED_MOVIES = 203;

    private static final UriMatcher URI_MATCHER = buildUriMatcher();
    private static final String FAILED_TO_INSERT_ROW_INTO = "Failed to insert row into ";

    // movies._id = ?
    private static final String MOVIE_ID_SELECTION =
            MoviesConnect.MovieEntry.TABLE_NAME + "." + MoviesConnect.MovieEntry._ID + " = ? ";


    private MoviesLocalDatabase moviesLocalDatabase;

    static UriMatcher buildUriMatcher() {
        final UriMatcher uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
        final String authority = MoviesConnect.CONTENT_AUTHORITY;

        uriMatcher.addURI(authority, MoviesConnect.PATH_MOVIES, MOVIES);
        uriMatcher.addURI(authority, MoviesConnect.PATH_MOVIES + "/#", MOVIE_BY_ID);

        uriMatcher.addURI(authority, MoviesConnect.PATH_MOVIES + "/" +
                MoviesConnect.PATH_MOST_POPULAR, MOST_POPULAR_MOVIES);
        uriMatcher.addURI(authority, MoviesConnect.PATH_MOVIES + "/" +
                MoviesConnect.PATH_MOST_RATED, MOST_RATED_MOVIES);

        return uriMatcher;
    }

    @Override
    public boolean onCreate() {
        moviesLocalDatabase = new MoviesLocalDatabase(getContext());
        return true;
    }

    @Override
    public String getType(@NonNull Uri uri) {
        final int match = URI_MATCHER.match(uri);
        switch (match) {
            case MOVIES:
                return MoviesConnect.MovieEntry.CONTENT_DIR_TYPE;
            case MOVIE_BY_ID:
                return MoviesConnect.MovieEntry.CONTENT_ITEM_TYPE;
            case MOST_POPULAR_MOVIES:
                return MoviesConnect.MostPopularMovies.CONTENT_DIR_TYPE;
            case MOST_RATED_MOVIES:
                return MoviesConnect.MostRatedMovies.CONTENT_DIR_TYPE;
            default:
                return null;
        }
    }

    @Override
    public Cursor query(@NonNull Uri uri, String[] projection, String selection,
                        String[] selectionArgs, String sortOrder) {
        int match = URI_MATCHER.match(uri);
        Cursor cursor;
        checkColumns(projection);
        switch (match) {
            case MOVIES:
                cursor = moviesLocalDatabase.getReadableDatabase().query(
                        MoviesConnect.MovieEntry.TABLE_NAME,
                        projection,
                        selection,
                        selectionArgs,
                        null,
                        null,
                        sortOrder
                );
                break;
            case MOVIE_BY_ID:
                cursor = getMovieById(uri, projection, sortOrder);
                break;
            case MOST_POPULAR_MOVIES:
                cursor = getMoviesFromSortTable(MoviesConnect.MostPopularMovies.TABLE_NAME,
                        projection, selection, selectionArgs, sortOrder);
                break;
            case MOST_RATED_MOVIES:
                cursor = getMoviesFromSortTable(MoviesConnect.MostRatedMovies.TABLE_NAME,
                        projection, selection, selectionArgs, sortOrder);
                break;
            default:
                return null;
        }
        cursor.setNotificationUri(getContext().getContentResolver(), uri);
        return cursor;
    }

    @Override
    public Uri insert(@NonNull Uri uri, ContentValues values) {
        SQLiteDatabase db = moviesLocalDatabase.getWritableDatabase();
        final int match = URI_MATCHER.match(uri);
        Uri returnUri;
        long id;
        switch (match) {
            case MOVIES:
                id = db.insertWithOnConflict(MoviesConnect.MovieEntry.TABLE_NAME, null,
                        values, SQLiteDatabase.CONFLICT_REPLACE);
                if (id > 0) {
                    returnUri = MoviesConnect.MovieEntry.buildMovieUri(id);
                } else {
                    throw new android.database.SQLException(FAILED_TO_INSERT_ROW_INTO + uri);
                }
                break;
            case MOST_POPULAR_MOVIES:
                id = db.insert(MoviesConnect.MostPopularMovies.TABLE_NAME, null, values);
                if (id > 0) {
                    returnUri = MoviesConnect.MostPopularMovies.CONTENT_URI;
                } else {
                    throw new android.database.SQLException(FAILED_TO_INSERT_ROW_INTO + uri);
                }
                break;
            case MOST_RATED_MOVIES:
                id = db.insert(MoviesConnect.MostRatedMovies.TABLE_NAME, null, values);
                if (id > 0) {
                    returnUri = MoviesConnect.MostRatedMovies.CONTENT_URI;
                } else {
                    throw new android.database.SQLException(FAILED_TO_INSERT_ROW_INTO + uri);
                }
                break;
            default:
                throw new UnsupportedOperationException("Unknown uri: " + uri);
        }
        getContext().getContentResolver().notifyChange(uri, null);
        return returnUri;
    }

    @Override
    public int update(@NonNull Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        SQLiteDatabase db = moviesLocalDatabase.getWritableDatabase();
        final int match = URI_MATCHER.match(uri);
        int rowsUpdated;
        switch (match) {
            case MOVIES:
                rowsUpdated = db.update(MoviesConnect.MovieEntry.TABLE_NAME, values,
                        selection, selectionArgs);
                break;
            default:
                throw new UnsupportedOperationException("Unknown uri: " + uri);
        }
        if (rowsUpdated != 0) {
            getContext().getContentResolver().notifyChange(uri, null);
        }
        return rowsUpdated;
    }

    @Override
    public int delete(@NonNull Uri uri, String selection, String[] selectionArgs) {
        SQLiteDatabase db = moviesLocalDatabase.getWritableDatabase();
        final int match = URI_MATCHER.match(uri);
        int rowsDeleted;
        switch (match) {
            case MOVIES:
                rowsDeleted = db.delete(MoviesConnect.MovieEntry.TABLE_NAME, selection, selectionArgs);
                break;
            case MOVIE_BY_ID:
                long id = MoviesConnect.MovieEntry.getIdFromUri(uri);
                rowsDeleted = db.delete(MoviesConnect.MovieEntry.TABLE_NAME,
                        MOVIE_ID_SELECTION, new String[]{Long.toString(id)});

                break;
            case MOST_POPULAR_MOVIES:
                rowsDeleted = db.delete(MoviesConnect.MostPopularMovies.TABLE_NAME, selection, selectionArgs);
                break;
            case MOST_RATED_MOVIES:
                rowsDeleted = db.delete(MoviesConnect.MostRatedMovies.TABLE_NAME, selection, selectionArgs);
                break;
            default:
                throw new UnsupportedOperationException("Unknown uri: " + uri);
        }
        if (rowsDeleted != 0) {
            getContext().getContentResolver().notifyChange(uri, null);
        }
        return rowsDeleted;
    }

    @Override
    public void shutdown() {
        moviesLocalDatabase.close();
        super.shutdown();
    }

    @Override
    public int bulkInsert(@NonNull Uri uri, @NonNull ContentValues[] values) {
        SQLiteDatabase db = moviesLocalDatabase.getWritableDatabase();
        final int match = URI_MATCHER.match(uri);
        switch (match) {
            case MOVIES:
                db.beginTransaction();
                int returnCount = 0;
                try {
                    for (ContentValues value : values) {
                        long id = db.insertWithOnConflict(MoviesConnect.MovieEntry.TABLE_NAME,
                                null, value, SQLiteDatabase.CONFLICT_REPLACE);
                        if (id != -1) {
                            returnCount++;
                        }
                    }
                    db.setTransactionSuccessful();
                } finally {
                    db.endTransaction();
                }
                getContext().getContentResolver().notifyChange(uri, null);
                return returnCount;
            default:
                return super.bulkInsert(uri, values);
        }
    }

    private Cursor getMovieById(Uri uri, String[] projection, String sortOrder) {
        long id = MoviesConnect.MovieEntry.getIdFromUri(uri);
        String selection = MOVIE_ID_SELECTION;
        String[] selectionArgs = new String[]{Long.toString(id)};
        return moviesLocalDatabase.getReadableDatabase().query(
                MoviesConnect.MovieEntry.TABLE_NAME,
                projection,
                selection,
                selectionArgs,
                null,
                null,
                sortOrder
        );
    }

    private Cursor getMoviesFromSortTable(String tableName, String[] projection, String selection,
                                          String[] selectionArgs, String sortOrder) {

        SQLiteQueryBuilder sqLiteQueryBuilder = new SQLiteQueryBuilder();

        // tableName INNER JOIN movies ON tableName.movie_id = movies._id
        sqLiteQueryBuilder.setTables(
                tableName + " INNER JOIN " + MoviesConnect.MovieEntry.TABLE_NAME +
                        " ON " + tableName + "." + MoviesConnect.COLUMN_MOVIE_ID_KEY +
                        " = " + MoviesConnect.MovieEntry.TABLE_NAME + "." + MoviesConnect.MovieEntry._ID
        );

        return sqLiteQueryBuilder.query(moviesLocalDatabase.getReadableDatabase(),
                projection,
                selection,
                selectionArgs,
                null,
                null,
                sortOrder
        );
    }

    private void checkColumns(String[] projection) {
        if (projection != null) {
            HashSet<String> availableColumns = new HashSet<>(Arrays.asList(
                    MoviesConnect.MovieEntry.getColumns()));
            HashSet<String> requestedColumns = new HashSet<>(Arrays.asList(projection));
            if (!availableColumns.containsAll(requestedColumns)) {
                throw new IllegalArgumentException("Unknown columns in projection.");
            }
        }
    }

}
