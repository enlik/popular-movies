package com.enliktjioe.popularmovie.data;

import android.content.ContentValues;
import android.database.Cursor;

import com.google.gson.annotations.SerializedName;

public class MoviePreferences {

    @SerializedName("id")
    private long id;

    @SerializedName("original_title")
    private String originalTitle;

    @SerializedName("overview")
    private String overview;

    @SerializedName("release_date")
    private String releaseDate;

    @SerializedName("poster_path")
    private String posterPath;

    @SerializedName("popularity")
    private double popularity;

    @SerializedName("title")
    private String title;

    @SerializedName("vote_average")
    private double averageVote;

    @SerializedName("vote_count")
    private long voteCount;

    @SerializedName("backdrop_path")
    private String backdropPath;

    @SerializedName("videos")
    private String videos;

    @SerializedName("reviews")
    private String reviews;

    public MoviePreferences(long id, String title) {
        this.id = id;
        this.title = title;
    }

    public static MoviePreferences fromCursor(Cursor cursor) {
        long id = cursor.getLong(cursor.getColumnIndex(MoviesConnect.MovieEntry._ID));
        String title = cursor.getString(cursor.getColumnIndex(MoviesConnect.MovieEntry.COLUMN_TITLE));
        MoviePreferences moviePreferences = new MoviePreferences(id, title);
        moviePreferences.setOriginalTitle(
                cursor.getString(cursor.getColumnIndex(MoviesConnect.MovieEntry.COLUMN_ORIGINAL_TITLE)));
        moviePreferences.setOverview(
                cursor.getString(cursor.getColumnIndex(MoviesConnect.MovieEntry.COLUMN_OVERVIEW)));
        moviePreferences.setReleaseDate(
                cursor.getString(cursor.getColumnIndex(MoviesConnect.MovieEntry.COLUMN_RELEASE_DATE)));
        moviePreferences.setPosterPath(
                cursor.getString(cursor.getColumnIndex(MoviesConnect.MovieEntry.COLUMN_POSTER_PATH)));
        moviePreferences.setPopularity(
                cursor.getDouble(cursor.getColumnIndex(MoviesConnect.MovieEntry.COLUMN_POPULARITY)));
        moviePreferences.setAverageVote(
                cursor.getDouble(cursor.getColumnIndex(MoviesConnect.MovieEntry.COLUMN_AVERAGE_VOTE)));
        moviePreferences.setVoteCount(
                cursor.getLong(cursor.getColumnIndex(MoviesConnect.MovieEntry.COLUMN_VOTE_COUNT)));
        moviePreferences.setBackdropPath(
                cursor.getString(cursor.getColumnIndex(MoviesConnect.MovieEntry.COLUMN_BACKDROP_PATH)));
//       TODO moviePreferences.setVideos(
//                cursor.getString(cursor)
//        );
        return moviePreferences;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getOriginalTitle() {
        return originalTitle;
    }

    public void setOriginalTitle(String originalTitle) {
        this.originalTitle = originalTitle;
    }

    public String getOverview() {
        return overview;
    }

    public void setOverview(String overview) {
        this.overview = overview;
    }

    public String getReleaseDate() {
        return releaseDate;
    }

    public void setReleaseDate(String releaseDate) {
        this.releaseDate = releaseDate;
    }

    public String getPosterPath() {
        return posterPath;
    }

    public void setPosterPath(String posterPath) {
        this.posterPath = posterPath;
    }

    public double getPopularity() {
        return popularity;
    }

    public void setPopularity(double popularity) {
        this.popularity = popularity;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public double getAverageVote() {
        return averageVote;
    }

    public void setAverageVote(double averageVote) {
        this.averageVote = averageVote;
    }

    public long getVoteCount() {
        return voteCount;
    }

    public void setVoteCount(long voteCount) {
        this.voteCount = voteCount;
    }

    public String getBackdropPath() {
        return backdropPath;
    }

    public void setBackdropPath(String backdropPath) {
        this.backdropPath = backdropPath;
    }

    public String getVideos() { return videos; }

    public void setVideos(String videos) { this.videos = videos; }

    public String getReviews() { return reviews; }

    public void setReviews(String reviews) { this.reviews = reviews; }

    @Override
    public String toString() {
        return "[MOVIE]: " + "id: " + this.id + "title: " + this.title;
    }

    public ContentValues toContentValues() {
        ContentValues values = new ContentValues();
        values.put(MoviesConnect.MovieEntry._ID, id);
        values.put(MoviesConnect.MovieEntry.COLUMN_ORIGINAL_TITLE, originalTitle);
        values.put(MoviesConnect.MovieEntry.COLUMN_OVERVIEW, overview);
        values.put(MoviesConnect.MovieEntry.COLUMN_RELEASE_DATE, releaseDate);
        values.put(MoviesConnect.MovieEntry.COLUMN_POSTER_PATH, posterPath);
        values.put(MoviesConnect.MovieEntry.COLUMN_POPULARITY, popularity);
        values.put(MoviesConnect.MovieEntry.COLUMN_TITLE, title);
        values.put(MoviesConnect.MovieEntry.COLUMN_AVERAGE_VOTE, averageVote);
        values.put(MoviesConnect.MovieEntry.COLUMN_VOTE_COUNT, voteCount);
        values.put(MoviesConnect.MovieEntry.COLUMN_BACKDROP_PATH, backdropPath);
//       TODO values.put(MoviesConnect.MovieEntry.);
        return values;
    }
}
