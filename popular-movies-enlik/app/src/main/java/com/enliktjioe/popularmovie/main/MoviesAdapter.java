package com.enliktjioe.popularmovie.main;


import android.annotation.SuppressLint;
import android.content.Context;
import android.database.Cursor;
import android.graphics.drawable.ColorDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import com.enliktjioe.popularmovie.utilities.CursorRecyclerViewAdapter;
import com.enliktjioe.popularmovie.utilities.OnItemClickListener;
import com.enliktjioe.popularmovie.R;
import com.enliktjioe.popularmovie.data.MoviePreferences;

public class MoviesAdapter extends CursorRecyclerViewAdapter<MovieItemViewHolder> {

    private static final String POSTER_IMAGE_BASE_URL = "https://image.tmdb.org/t/p/";
    private static final String POSTER_IMAGE_SIZE = "w185";
    private final Context context;
    private OnItemClickListener onItemClickListener;

    public MoviesAdapter(Context context, Cursor cursor) {
        super(cursor);
        this.context = context;
    }

    public void setOnItemClickListener(OnItemClickListener listener) {
        this.onItemClickListener = listener;
    }

    @SuppressLint("PrivateResource")
    @Override
    public void onBindViewHolder(MovieItemViewHolder viewHolder, Cursor cursor) {
        if (cursor != null) {
            MoviePreferences moviePreferences = MoviePreferences.fromCursor(cursor);
            viewHolder.moviePoster.setContentDescription(moviePreferences.getTitle());
            Glide.with(context)
                    .load(POSTER_IMAGE_BASE_URL + POSTER_IMAGE_SIZE + moviePreferences.getPosterPath())
                    .placeholder(new ColorDrawable(context.getColor(R.color.colorPrimaryDark)))
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .centerCrop()
                    .crossFade()
                    .into(viewHolder.moviePoster);
        }

    }

    @Override
    public MovieItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.movie_item, parent, false);
        return new MovieItemViewHolder(itemView, onItemClickListener);
    }

}
