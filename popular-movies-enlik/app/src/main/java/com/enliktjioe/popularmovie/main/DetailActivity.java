package com.enliktjioe.popularmovie.main;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.ShareCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import butterknife.BindView;
import butterknife.ButterKnife;

import com.enliktjioe.popularmovie.R;
import com.enliktjioe.popularmovie.data.MoviePreferences;
import com.enliktjioe.popularmovie.data.MoviesConnect;

public class DetailActivity extends AppCompatActivity {

    private static final String POSTER_IMAGE_BASE_URL = "https://image.tmdb.org/t/p/";
    private static final String POSTER_IMAGE_SIZE = "w780";

    private static final String MOVIE_SHARE_HASHTAG = "#PopularMovieApp";
    private static final String MOVIE_SHARE_BASE_URL = "https://www.themoviedb.org/movie/";


    @BindView(R.id.image_movie_detail_poster)
    ImageView movieImagePoster;
    @BindView(R.id.text_movie_original_title)
    TextView movieOriginalTitle;
    @BindView(R.id.text_movie_user_rating)
    TextView movieUserRating;
    @BindView(R.id.text_movie_release_date)
    TextView movieReleaseDate;
    @BindView(R.id.text_movie_overview)
    TextView movieOverview;
    @BindView(R.id.toolbar)
    Toolbar toolbar;

    private MoviePreferences moviePreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        ButterKnife.bind(this);
        initToolbar();
        initMovie();
        initViews();
    }

    private void initMovie() {
        long movieId = getIntent().getLongExtra("movieId", 0);
        Cursor cursor = getContentResolver().query(
                MoviesConnect.MovieEntry.buildMovieUri(movieId),
                null,
                null,
                null,
                null
        );
        if (cursor != null) {
            cursor.moveToFirst();
            moviePreferences = MoviePreferences.fromCursor(cursor);
            cursor.close();
        }
    }

    private void initToolbar() {
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onBackPressed();
                }
            });
        }
    }

    private void initViews() {
        setTitle(moviePreferences.getTitle());
        Glide.with(this)
                .load(POSTER_IMAGE_BASE_URL + POSTER_IMAGE_SIZE + moviePreferences.getPosterPath())
                .crossFade()
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(movieImagePoster);
        movieOriginalTitle.setText(moviePreferences.getOriginalTitle());
        String userRating = String.format(getString(com.enliktjioe.popularmovie.R.string.rating),
                moviePreferences.getAverageVote());
        movieUserRating.setText(userRating);
        String releaseDate = String.format(getString(com.enliktjioe.popularmovie.R.string.release_date),
                moviePreferences.getReleaseDate());
        movieReleaseDate.setText(releaseDate);
        movieOverview.setText(moviePreferences.getOverview());
    }

    private Intent createShareMovieIntent() {
        String movieTitleForShare = moviePreferences.getOriginalTitle();
        String movieIdForShareURL = String.format("%s", moviePreferences.getId());
        Intent shareIntent = ShareCompat.IntentBuilder.from(this)
                .setType("text/plain")
                .setText(MOVIE_SHARE_HASHTAG + " " + movieTitleForShare + " " + MOVIE_SHARE_BASE_URL + movieIdForShareURL)
                .getIntent();
        return shareIntent;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.detail, menu);
        MenuItem menuItem = menu.findItem(R.id.action_share);
        menuItem.setIntent(createShareMovieIntent());
        return true;
    }

}
