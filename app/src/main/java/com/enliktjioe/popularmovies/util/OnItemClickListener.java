package com.enliktjioe.popularmovies.util;

import android.view.View;

public interface OnItemClickListener {
    void onItemClick(View itemView, int position);
}
