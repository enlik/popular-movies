package com.enliktjioe.popularmovies.tmdb;

import android.util.Log;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

public class LoggingInterceptor implements Interceptor {

    public static final double TIME_DIVIDER = 1e6d;

    @Override
    public Response intercept(Chain chain) throws IOException {
        Request request = chain.request();

        long t1 = System.nanoTime();
        Log.i("OkHTTP_Request", String.format("Sending request %s on %s%n%s",
                request.url(), chain.connection(), request.headers()));

        Response response = chain.proceed(request);

        long t2 = System.nanoTime();
        Log.i("OkHTTP_Response", String.format("Received response for %s in %.1fms%n%s",
                response.request().url(), (t2 - t1) / TIME_DIVIDER, response.headers()));

        return response;
    }
}