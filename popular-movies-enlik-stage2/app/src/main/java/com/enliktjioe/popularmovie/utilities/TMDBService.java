package com.enliktjioe.popularmovie.utilities;

import com.enliktjioe.popularmovie.data.MoviePreferences;

import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;
import rx.Observable;

public interface TMDBService {

    @GET("movie/{id}/videos")
    Observable<ResponseMovieVideo> getMovieVideos(@Path("id") long movieId);

    @GET("movie/{id}/reviews")
    Observable<ResponeMovieReview> getMovieReviews(@Path("id") long movieId);

    @GET("movie/{sort_by}")
    Observable<ResponseUtils<MoviePreferences>> discoverMovies(@Path("sort_by") String sortBy, @Query("page") Integer page);

}
