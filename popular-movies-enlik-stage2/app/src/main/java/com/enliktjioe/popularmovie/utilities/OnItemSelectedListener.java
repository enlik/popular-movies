package com.enliktjioe.popularmovie.utilities;

import com.enliktjioe.popularmovie.data.MoviePreferences;

public interface OnItemSelectedListener {
    void onItemSelected(MoviePreferences moviePreferences);
}
