package com.enliktjioe.popularmovie.data;

import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.enliktjioe.popularmovie.utilities.ResponseUtils;
import com.enliktjioe.popularmovie.utilities.TMBDClient;
import com.enliktjioe.popularmovie.utilities.TMDBService;
import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class MoviesService {

    public static final String BROADCAST_UPDATE_FINISHED = "UpdateFinished";
    public static final String EXTRA_IS_SUCCESSFUL_UPDATED = "isSuccessfulUpdated";

    private static final int PAGE_SIZE = 20;
    private static final String LOG_TAG = "MoviesService";
    private static volatile MoviesService instance = null;

    private SortUtil sortUtil;
    private final Context context;
    private volatile boolean loading = false;

    public MoviesService(Context context) {
        if (instance != null) {
            throw new IllegalStateException("Already instantiated.");
        }
        this.context = context.getApplicationContext();
        sortUtil = new SortUtil(PreferenceManager.getDefaultSharedPreferences(context));
    }

    public static MoviesService getInstance(Context context) {
        synchronized (MoviesService.class) {
            if (instance == null) {
                instance = new MoviesService(context);
            }
        }
        return instance;
    }

    public void refreshMovies() {
        if (loading) {
            return;
        }
        loading = true;

        String sort = sortUtil.getSortByPreference().toString();
        callDiscoverMovies(sort, null);
    }

    public boolean isLoading() {
        return loading;
    }

    public void loadMoreMovies() {
        if (loading) {
            return;
        }
        loading = true;
        String sort = sortUtil.getSortByPreference().toString();
        Uri uri = sortUtil.getSortedMoviesUri();
        if (uri == null) {
            return;
        }
        callDiscoverMovies(sort, getCurrentPage(uri) + 1);
    }

    private void callDiscoverMovies(String sort, @Nullable Integer page) {
        TMDBService service = TMBDClient.getInstance(context);

        service.discoverMovies(sort, page)
                .subscribeOn(Schedulers.newThread())
                .doOnNext(discoverMoviesResponse -> clearMoviesSortTableIfNeeded(discoverMoviesResponse))
                .doOnNext(discoverMoviesResponse -> logResponse(discoverMoviesResponse))
                .map(discoverMoviesResponse -> discoverMoviesResponse.getResults())
                .flatMap(movies -> Observable.from(movies))
                .map(movie -> saveMovie(movie))
                .map(movieUri -> MoviesConnect.MovieEntry.getIdFromUri(movieUri))
                .doOnNext(movieId -> saveMovieReference(movieId))
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<Long>() {
                    @Override
                    public void onCompleted() {
                        loading = false;
                        sendUpdateFinishedBroadcast(true);
                    }

                    @Override
                    public void onError(Throwable e) {
                        loading = false;
                        sendUpdateFinishedBroadcast(false);
                    }

                    @Override
                    public void onNext(Long aLong) {
                        // do nothing
                    }
                });
    }

    private void saveMovieReference(Long movieId) {
        ContentValues entry = new ContentValues();
        entry.put(MoviesConnect.COLUMN_MOVIE_ID_KEY, movieId);
        context.getContentResolver().insert(sortUtil.getSortedMoviesUri(), entry);
    }

    private Uri saveMovie(MoviePreferences moviePreferences) {
        return context.getContentResolver().insert(MoviesConnect.MovieEntry.CONTENT_URI, moviePreferences.toContentValues());
    }

    private void logResponse(ResponseUtils<MoviePreferences> discoverMoviesResponse) {
        Log.d(LOG_TAG, "page == " + discoverMoviesResponse.getPage() + " " +
                discoverMoviesResponse.getResults().toString());
    }

    private void clearMoviesSortTableIfNeeded(ResponseUtils<MoviePreferences> discoverMoviesResponse) {
        if (discoverMoviesResponse.getPage() == 1) {
            context.getContentResolver().delete(
                    sortUtil.getSortedMoviesUri(),
                    null,
                    null
            );
        }
    }

    private void sendUpdateFinishedBroadcast(boolean successfulUpdated) {
        Intent intent = new Intent(BROADCAST_UPDATE_FINISHED);
        intent.putExtra(EXTRA_IS_SUCCESSFUL_UPDATED, successfulUpdated);
        LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
    }

    private int getCurrentPage(Uri uri) {
        Cursor movies = context.getContentResolver().query(
                uri,
                null,
                null,
                null,
                null
        );

        int currentPage = 1;
        if (movies != null) {
            currentPage = (movies.getCount() - 1) / PAGE_SIZE + 1;
            movies.close();
        }
        return currentPage;
    }
}
