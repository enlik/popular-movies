package com.enliktjioe.popularmovie.utilities;

import android.view.View;

public interface OnItemClickListener {
    void onItemClick(View itemView, int position);
}
