package com.enliktjioe.popularmovie.data;

import android.content.SharedPreferences;
import android.net.Uri;

public final class SortUtil {

    private static final String PREF_SORT_BY_KEY = "sortBy";
    private static final String PREF_SORT_BY_DEFAULT_VALUE = "popular";

    private SharedPreferences sharedPreferences;

    public SortUtil(SharedPreferences sharedPreferences) {
        this.sharedPreferences = sharedPreferences;
    }

    public SortCommon getSortByPreference() {
        String sort = sharedPreferences.getString(
                PREF_SORT_BY_KEY,
                PREF_SORT_BY_DEFAULT_VALUE
        );
        return SortCommon.fromString(sort);
    }

    public Uri getSortedMoviesUri() {
        SortCommon sortCommon = getSortByPreference();
        switch (sortCommon) {
            case POPULAR:
                return MoviesConnect.MostPopularMovies.CONTENT_URI;
            case TOP_RATED:
                return MoviesConnect.MostRatedMovies.CONTENT_URI;
            default:
                throw new IllegalStateException("Unknown sortCommon.");
        }
    }

    public boolean saveSortByPreference(SortCommon sortCommon) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(
                PREF_SORT_BY_KEY,
                sortCommon.toString()
        );
        return editor.commit();
    }
}
