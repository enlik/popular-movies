package com.enliktjioe.popularmovie.data;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Manages a local database for movies.
 */
public class MoviesLocalDatabase extends SQLiteOpenHelper {

    static final String DATABASE_NAME = "movies.db";
    private static final int DATABASE_SCHEMA_VERSION = 4;
    private static final String SQL_DROP_TABLE_IF_EXISTS = "DROP TABLE IF EXISTS ";

    public MoviesLocalDatabase(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_SCHEMA_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(MoviesConnect.MovieEntry.SQL_CREATE_TABLE);
        db.execSQL(MoviesConnect.MostPopularMovies.SQL_CREATE_TABLE);
        db.execSQL(MoviesConnect.HighestRatedMovies.SQL_CREATE_TABLE);
        db.execSQL(MoviesConnect.MostRatedMovies.SQL_CREATE_TABLE);
        db.execSQL(MoviesConnect.Favorites.SQL_CREATE_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(SQL_DROP_TABLE_IF_EXISTS + MoviesConnect.MovieEntry.TABLE_NAME);
        db.execSQL(SQL_DROP_TABLE_IF_EXISTS + MoviesConnect.MostPopularMovies.TABLE_NAME);
        db.execSQL(SQL_DROP_TABLE_IF_EXISTS + MoviesConnect.HighestRatedMovies.TABLE_NAME);
        db.execSQL(SQL_DROP_TABLE_IF_EXISTS + MoviesConnect.MostRatedMovies.TABLE_NAME);
        db.execSQL(SQL_DROP_TABLE_IF_EXISTS + MoviesConnect.Favorites.TABLE_NAME);
        onCreate(db);
    }
}
