package com.enliktjioe.popularmovie.data;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

public class FavoritesService {

    private static volatile FavoritesService instance = null;

    private final Context context;

    public FavoritesService(Context context) {
        if (instance != null) {
            throw new IllegalStateException("Already instantiated.");
        }
        this.context = context.getApplicationContext();
    }

    public static FavoritesService getInstance(Context context) {
        synchronized (FavoritesService.class) {
            if (instance == null) {
                instance = new FavoritesService(context);
            }
        }
        return instance;
    }

    public void addToFavorites(MoviePreferences moviePreferences) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(MoviesConnect.COLUMN_MOVIE_ID_KEY, moviePreferences.getId());
        context.getContentResolver().insert(MoviesConnect.Favorites.CONTENT_URI, contentValues);
    }

    public void removeFromFavorites(MoviePreferences moviePreferences) {
        context.getContentResolver().delete(
                MoviesConnect.Favorites.CONTENT_URI,
                MoviesConnect.COLUMN_MOVIE_ID_KEY + " = " + moviePreferences.getId(),
                null
        );
    }

    public boolean isFavorite(MoviePreferences moviePreferences) {
        boolean favorite = false;
        Cursor cursor = context.getContentResolver().query(
                MoviesConnect.Favorites.CONTENT_URI,
                null,
                MoviesConnect.COLUMN_MOVIE_ID_KEY + " = " + moviePreferences.getId(),
                null,
                null
        );
        if (cursor != null) {
            favorite = cursor.getCount() != 0;
            cursor.close();
        }
        return favorite;
    }
}
