package com.enliktjioe.popularmovie.main;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ScrollView;

import com.enliktjioe.popularmovie.data.FavoritesService;
import com.enliktjioe.popularmovie.data.MoviePreferences;
import com.enliktjioe.popularmovie.data.SortUtil;
import com.enliktjioe.popularmovie.main.detail.MovieDetailsActivity;
import com.enliktjioe.popularmovie.main.detail.MovieDetailsFragment;
import com.enliktjioe.popularmovie.main.grid.FavoritesGrid;
import com.enliktjioe.popularmovie.main.grid.MoviesGrid;
import com.enliktjioe.popularmovie.utilities.OnItemSelectedListener;
import com.enliktjioe.popularmovie.utilities.SortDialogFragment;

import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Optional;

public class MainActivity extends AppCompatActivity implements OnItemSelectedListener,
        NavigationView.OnNavigationItemSelectedListener {

    private static final String SELECTED_MOVIE_KEY = "MovieSelected";
    private static final String SELECTED_NAVIGATION_ITEM_KEY = "SelectedNavigationItem";

    @BindView(com.enliktjioe.popularmovie.R.id.drawer_layout)
    DrawerLayout drawerLayout;
    @BindView(com.enliktjioe.popularmovie.R.id.navigation_view)
    NavigationView navigationView;
    @BindView(com.enliktjioe.popularmovie.R.id.coordinator_layout)
    CoordinatorLayout coordinatorLayout;
    @BindView(com.enliktjioe.popularmovie.R.id.toolbar)
    Toolbar toolbar;
    @Nullable
    @BindView(com.enliktjioe.popularmovie.R.id.movie_detail_container)
    ScrollView movieDetailContainer;
    @Nullable
    @BindView(com.enliktjioe.popularmovie.R.id.fab)
    FloatingActionButton fab;

    private FavoritesService favoritesService;
    private SortUtil sortUtil;
    private boolean twoPaneMode;
    private MoviePreferences selectedMoviePreferences = null;
    private int selectedNavigationItem;

    private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (action.equals(SortDialogFragment.BROADCAST_SORT_PREFERENCE_CHANGED)) {
                hideMovieDetailContainer();
                updateTitle();
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(com.enliktjioe.popularmovie.R.layout.activity_main);
        ButterKnife.bind(this);
        favoritesService = FavoritesService.getInstance(this);
        sortUtil = new SortUtil(PreferenceManager.getDefaultSharedPreferences(this));
        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .replace(com.enliktjioe.popularmovie.R.id.movies_grid_container, MoviesGrid.create())
                    .commit();
        }
        twoPaneMode = movieDetailContainer != null;
        if (twoPaneMode && selectedMoviePreferences == null) {
            movieDetailContainer.setVisibility(View.GONE);
        }
        setupToolbar();
        setupNavigationDrawer();
        setupNavigationView();
        setupFab();
    }

    @Override
    protected void onResume() {
        super.onResume();
        IntentFilter intentFilter = new IntentFilter(SortDialogFragment.BROADCAST_SORT_PREFERENCE_CHANGED);
        LocalBroadcastManager.getInstance(this).registerReceiver(broadcastReceiver, intentFilter);
        updateTitle();
    }

    @Override
    protected void onPause() {
        super.onPause();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(broadcastReceiver);
    }

    private void updateTitle() {
        if (selectedNavigationItem == 0) {
            String[] sortTitles = getResources().getStringArray(com.enliktjioe.popularmovie.R.array.sort_types);
            int currentSortIndex = sortUtil.getSortByPreference().ordinal();
            String title = Character.toString(sortTitles[currentSortIndex].charAt(0)).toUpperCase(Locale.US) +
                    sortTitles[currentSortIndex].substring(1);
            setTitle(title);
        } else if (selectedNavigationItem == 1) {
            setTitle(getResources().getString(com.enliktjioe.popularmovie.R.string.favorites_grid_title));
        }
    }

    private void setupFab() {
        if (fab != null) {
            if (twoPaneMode && selectedMoviePreferences != null) {
                if (favoritesService.isFavorite(selectedMoviePreferences)) {
                    fab.setImageResource(com.enliktjioe.popularmovie.R.drawable.ic_favorite_white);
                } else {
                    fab.setImageResource(com.enliktjioe.popularmovie.R.drawable.ic_favorite_white_border);
                }
                fab.show();
            } else {
                fab.hide();
            }
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelable(SELECTED_MOVIE_KEY, selectedMoviePreferences);
        outState.putInt(SELECTED_NAVIGATION_ITEM_KEY, selectedNavigationItem);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        if (savedInstanceState != null) {
            selectedMoviePreferences = savedInstanceState.getParcelable(SELECTED_MOVIE_KEY);
            selectedNavigationItem = savedInstanceState.getInt(SELECTED_NAVIGATION_ITEM_KEY);
            Menu menu = navigationView.getMenu();
            menu.getItem(selectedNavigationItem).setChecked(true);
        }
    }

    @Override
    public void onBackPressed() {
        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawers();
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public void onItemSelected(MoviePreferences moviePreferences) {
        if (twoPaneMode && movieDetailContainer != null) {
            movieDetailContainer.setVisibility(View.VISIBLE);
            selectedMoviePreferences = moviePreferences;
            getSupportFragmentManager().beginTransaction()
                    .replace(com.enliktjioe.popularmovie.R.id.movie_detail_container, MovieDetailsFragment.create(moviePreferences))
                    .commit();
            setupFab();
        } else {
            MovieDetailsActivity.start(this, moviePreferences);
        }
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        item.setChecked(true);
        switch (item.getItemId()) {
            case com.enliktjioe.popularmovie.R.id.drawer_item_explore:
                if (selectedNavigationItem != 0) {
                    getSupportFragmentManager().beginTransaction()
                            .replace(com.enliktjioe.popularmovie.R.id.movies_grid_container, MoviesGrid.create())
                            .commit();
                    selectedNavigationItem = 0;
                    hideMovieDetailContainer();
                }
                drawerLayout.closeDrawers();
                updateTitle();
                return true;
            case com.enliktjioe.popularmovie.R.id.drawer_item_favorites:
                if (selectedNavigationItem != 1) {
                    getSupportFragmentManager().beginTransaction()
                            .replace(com.enliktjioe.popularmovie.R.id.movies_grid_container, FavoritesGrid.create())
                            .commit();
                    selectedNavigationItem = 1;
                    hideMovieDetailContainer();
                }
                drawerLayout.closeDrawers();
                updateTitle();
                return true;
            default:
                return false;
        }
    }

    @Optional
    @OnClick(com.enliktjioe.popularmovie.R.id.fab)
    void onFabClicked() {
        if (favoritesService.isFavorite(selectedMoviePreferences)) {
            favoritesService.removeFromFavorites(selectedMoviePreferences);
            showSnackbar(com.enliktjioe.popularmovie.R.string.message_removed_from_favorites);
            if (selectedNavigationItem == 1) {
                hideMovieDetailContainer();
            }
        } else {
            favoritesService.addToFavorites(selectedMoviePreferences);
            showSnackbar(com.enliktjioe.popularmovie.R.string.message_added_to_favorites);
        }
        setupFab();
    }

    private void showSnackbar(String message) {
        Snackbar.make(coordinatorLayout, message, Snackbar.LENGTH_LONG).show();
    }

    private void showSnackbar(@StringRes int messageResourceId) {
        showSnackbar(getString(messageResourceId));
    }

    private void hideMovieDetailContainer() {
        selectedMoviePreferences = null;
        setupFab();
        if (twoPaneMode && movieDetailContainer != null) {
            movieDetailContainer.setVisibility(View.GONE);
        }
    }

    private void setupToolbar() {
        setSupportActionBar(toolbar);
    }

    private void setupNavigationDrawer() {
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
        toolbar.setNavigationIcon(com.enliktjioe.popularmovie.R.drawable.ic_menu);
        toolbar.setNavigationOnClickListener(view -> drawerLayout.openDrawer(GravityCompat.START));
    }

    private void setupNavigationView() {
        navigationView.setNavigationItemSelectedListener(this);
    }
}
