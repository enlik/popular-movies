package com.enliktjioe.popularmovie.utilities;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;

import com.enliktjioe.popularmovie.R;
import com.enliktjioe.popularmovie.data.SortCommon;
import com.enliktjioe.popularmovie.data.SortUtil;

public class SortDialogFragment extends DialogFragment {

    public static final String BROADCAST_SORT_PREFERENCE_CHANGED = "SortPreferenceChanged";

    public static final String TAG = "SortDialogFragment";

    private SortUtil sortUtil;

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        sortUtil = new SortUtil(PreferenceManager.getDefaultSharedPreferences(getContext()));
        final AlertDialog.Builder builder = new AlertDialog.Builder(getContext(), R.style.DialogStyle);
        builder.setTitle(getString(R.string.sort_dialog_title));
        builder.setNegativeButton(getString(R.string.action_cancel), null);
        builder.setSingleChoiceItems(R.array.sort_types,
                sortUtil.getSortByPreference().ordinal(),
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int which) {
                        sortUtil.saveSortByPreference(SortCommon.values()[which]);
                        sendSortPreferenceChangedBroadcast();
                        dialogInterface.dismiss();
                    }
                });

        return builder.create();
    }

    private void sendSortPreferenceChangedBroadcast() {
        Intent intent = new Intent(BROADCAST_SORT_PREFERENCE_CHANGED);
        LocalBroadcastManager.getInstance(getContext()).sendBroadcast(intent);
    }
}
