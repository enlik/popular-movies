package com.enliktjioe.popularmovie.main.detail;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.StringRes;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ShareCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewCompat;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.view.Menu;
import android.view.MenuItem;

import com.enliktjioe.popularmovie.data.MoviePreferences;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import com.enliktjioe.popularmovie.R;
import com.enliktjioe.popularmovie.data.FavoritesService;

public class MovieDetailsActivity extends AppCompatActivity {

    private static final String ARG_MOVIE = "argMovie";

    private static final String MOVIE_SHARE_HASHTAG = "#PopularMovieApp";
    private static final String MOVIE_SHARE_BASE_URL = "https://www.themoviedb.org/movie/";


    @BindView(R.id.coordinator_layout)
    CoordinatorLayout coordinatorLayout;
    @BindView(R.id.collapsing_toolbar_layout)
    CollapsingToolbarLayout collapsingToolbarLayout;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.fab)
    FloatingActionButton fab;
    @BindView(R.id.nestedScrollView)
    NestedScrollView nestedScrollView;

    private FavoritesService favoritesService;
    private MoviePreferences moviePreferences;

    public static void start(Context context, MoviePreferences moviePreferences) {
        Intent intent = new Intent(context, MovieDetailsActivity.class);
        intent.putExtra(ARG_MOVIE, moviePreferences);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_movie_details);
        ButterKnife.bind(this);
        moviePreferences = getIntent().getParcelableExtra(ARG_MOVIE);
        favoritesService = FavoritesService.getInstance(this);
        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.movies_grid_container, MovieDetailsFragment.create(moviePreferences))
                    .commit();
        }
        initToolbar();
        ViewCompat.setElevation(nestedScrollView,
                convertDpToPixel(getResources().getInteger(R.integer.movie_detail_content_elevation_in_dp)));
        ViewCompat.setElevation(fab,
                convertDpToPixel(getResources().getInteger(R.integer.movie_detail_fab_elevation_in_dp)));
        updateFab();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @OnClick(R.id.fab)
    void onFabClicked() {
        if (favoritesService.isFavorite(moviePreferences)) {
            favoritesService.removeFromFavorites(moviePreferences);
            showSnackbar(R.string.message_removed_from_favorites);
        } else {
            favoritesService.addToFavorites(moviePreferences);
            showSnackbar(R.string.message_added_to_favorites);
        }
        updateFab();
    }


    private void updateFab() {
        if (favoritesService.isFavorite(moviePreferences)) {
            fab.setImageResource(R.drawable.ic_favorite_white);
        } else {
            fab.setImageResource(R.drawable.ic_favorite_white_border);
        }
    }

    private void initToolbar() {
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            toolbar.setNavigationOnClickListener(view -> onBackPressed());
        }
        collapsingToolbarLayout.setTitle(moviePreferences.getTitle());
        collapsingToolbarLayout.setExpandedTitleColor(ContextCompat.getColor(this, android.R.color.transparent));
        setTitle("");
    }

    private void showSnackbar(String message) {
        Snackbar.make(coordinatorLayout, message, Snackbar.LENGTH_LONG).show();
    }

    private void showSnackbar(@StringRes int messageResourceId) {
        showSnackbar(getString(messageResourceId));
    }

    public float convertDpToPixel(float dp) {
        DisplayMetrics metrics = getResources().getDisplayMetrics();
        return dp * ((float) metrics.densityDpi / DisplayMetrics.DENSITY_DEFAULT);
    }

    private Intent createShareMovieIntent() {
        String movieTitleForShare = moviePreferences.getOriginalTitle();
        String movieIdForShareURL = String.format("%s", moviePreferences.getId());
        Intent shareIntent = ShareCompat.IntentBuilder.from(this)
                .setType("text/plain")
                .setText(MOVIE_SHARE_HASHTAG + " " + movieTitleForShare + " " + MOVIE_SHARE_BASE_URL + movieIdForShareURL)
                .getIntent();
        return shareIntent;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.detail, menu);
        MenuItem menuItem = menu.findItem(R.id.action_share);
        menuItem.setIntent(createShareMovieIntent());
        return true;
    }

}
