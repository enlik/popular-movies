package com.enliktjioe.popularmovie.main.detail;

import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;

import com.enliktjioe.popularmovie.utilities.OnItemClickListener;

public class MovieReviewViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    @BindView(com.enliktjioe.popularmovie.R.id.text_movie_review_content)
    TextView content;
    @BindView(com.enliktjioe.popularmovie.R.id.text_movie_review_author)
    TextView author;

    @Nullable
    private OnItemClickListener onItemClickListener;

    public MovieReviewViewHolder(View itemView, @Nullable OnItemClickListener onItemClickListener) {
        super(itemView);
        ButterKnife.bind(this, itemView);
        this.onItemClickListener = onItemClickListener;
        itemView.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        if (onItemClickListener != null) {
            onItemClickListener.onItemClick(view, getAdapterPosition());
        }
    }
}
