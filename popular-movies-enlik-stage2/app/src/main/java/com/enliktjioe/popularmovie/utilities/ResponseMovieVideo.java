package com.enliktjioe.popularmovie.utilities;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

import com.enliktjioe.popularmovie.data.MovieVideo;

public class ResponseMovieVideo {
    @SerializedName("id")
    private long movieId;

    @SerializedName("results")
    private ArrayList<MovieVideo> results;

    public ResponseMovieVideo(long movieId, ArrayList<MovieVideo> results) {
        this.movieId = movieId;
        this.results = results;
    }

    public long getMovieId() {
        return movieId;
    }

    public ArrayList<MovieVideo> getResults() {
        return results;
    }
}
