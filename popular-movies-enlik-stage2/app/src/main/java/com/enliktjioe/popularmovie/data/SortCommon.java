package com.enliktjioe.popularmovie.data;

import android.support.annotation.NonNull;

public enum SortCommon {

    POPULAR("popular"),
    TOP_RATED("top_rated");

    private String value;

    SortCommon(String sort) {
        value = sort;
    }

    public static SortCommon fromString(@NonNull String string) {
        for (SortCommon sortCommon : SortCommon.values()) {
            if (string.equals(sortCommon.toString())) {
                return sortCommon;
            }
        }
        throw new IllegalArgumentException("No constant with text " + string + " found.");
    }

    @Override
    public String toString() {
        return value;
    }

}
