package com.enliktjioe.popularmovie.main.grid;

import android.database.Cursor;
import android.net.Uri;
import android.support.annotation.NonNull;

import com.enliktjioe.popularmovie.data.MoviesConnect;

public class FavoritesGrid extends AbstractMoviesGrid {

    public static FavoritesGrid create() {
        return new FavoritesGrid();
    }

    @Override
    @NonNull
    protected Uri getContentUri() {
        return MoviesConnect.Favorites.CONTENT_URI;
    }

    @Override
    protected void onCursorLoaded(Cursor data) {
        getAdapter().changeCursor(data);
    }

    @Override
    protected void onRefreshAction() {
        // do nothing
        swipeRefreshLayout.setRefreshing(false);
    }

    @Override
    protected void onMoviesGridInitialisationFinished() {
        // do nothing
    }
}
