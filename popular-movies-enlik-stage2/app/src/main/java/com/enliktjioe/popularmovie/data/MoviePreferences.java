package com.enliktjioe.popularmovie.data;

import android.content.ContentValues;
import android.database.Cursor;
import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

@SuppressWarnings("PMD.GodClass")
public class MoviePreferences implements Parcelable {

    public static final Parcelable.Creator<MoviePreferences> CREATOR = new Parcelable.Creator<MoviePreferences>() {
        @Override
        public MoviePreferences createFromParcel(Parcel source) {
            return new MoviePreferences(source);
        }

        @Override
        public MoviePreferences[] newArray(int size) {
            return new MoviePreferences[size];
        }
    };

    @SerializedName("id")
    private long id;

    @SerializedName("original_title")
    private String originalTitle;

    @SerializedName("overview")
    private String overview;

    @SerializedName("release_date")
    private String releaseDate;

    @SerializedName("poster_path")
    private String posterPath;

    @SerializedName("popularity")
    private double popularity;

    @SerializedName("title")
    private String title;

    @SerializedName("vote_average")
    private double averageVote;

    @SerializedName("vote_count")
    private long voteCount;

    @SerializedName("backdrop_path")
    private String backdropPath;

    public MoviePreferences(long id, String title) {
        this.id = id;
        this.title = title;
    }

    protected MoviePreferences(Parcel in) {
        this.id = in.readLong();
        this.originalTitle = in.readString();
        this.overview = in.readString();
        this.releaseDate = in.readString();
        this.posterPath = in.readString();
        this.popularity = in.readDouble();
        this.title = in.readString();
        this.averageVote = in.readDouble();
        this.voteCount = in.readLong();
        this.backdropPath = in.readString();
    }

    public static MoviePreferences fromCursor(Cursor cursor) {
        long id = cursor.getLong(cursor.getColumnIndex(MoviesConnect.MovieEntry._ID));
        String title = cursor.getString(cursor.getColumnIndex(MoviesConnect.MovieEntry.COLUMN_TITLE));
        MoviePreferences moviePreferences = new MoviePreferences(id, title);
        moviePreferences.setOriginalTitle(
                cursor.getString(cursor.getColumnIndex(MoviesConnect.MovieEntry.COLUMN_ORIGINAL_TITLE)));
        moviePreferences.setOverview(
                cursor.getString(cursor.getColumnIndex(MoviesConnect.MovieEntry.COLUMN_OVERVIEW)));
        moviePreferences.setReleaseDate(
                cursor.getString(cursor.getColumnIndex(MoviesConnect.MovieEntry.COLUMN_RELEASE_DATE)));
        moviePreferences.setPosterPath(
                cursor.getString(cursor.getColumnIndex(MoviesConnect.MovieEntry.COLUMN_POSTER_PATH)));
        moviePreferences.setPopularity(
                cursor.getDouble(cursor.getColumnIndex(MoviesConnect.MovieEntry.COLUMN_POPULARITY)));
        moviePreferences.setAverageVote(
                cursor.getDouble(cursor.getColumnIndex(MoviesConnect.MovieEntry.COLUMN_AVERAGE_VOTE)));
        moviePreferences.setVoteCount(
                cursor.getLong(cursor.getColumnIndex(MoviesConnect.MovieEntry.COLUMN_VOTE_COUNT)));
        moviePreferences.setBackdropPath(
                cursor.getString(cursor.getColumnIndex(MoviesConnect.MovieEntry.COLUMN_BACKDROP_PATH)));
        return moviePreferences;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getOriginalTitle() {
        return originalTitle;
    }

    public void setOriginalTitle(String originalTitle) {
        this.originalTitle = originalTitle;
    }

    public String getOverview() {
        return overview;
    }

    public void setOverview(String overview) {
        this.overview = overview;
    }

    public String getReleaseDate() {
        return releaseDate;
    }

    public void setReleaseDate(String releaseDate) {
        this.releaseDate = releaseDate;
    }

    public String getPosterPath() {
        return posterPath;
    }

    public void setPosterPath(String posterPath) {
        this.posterPath = posterPath;
    }

    public double getPopularity() {
        return popularity;
    }

    public void setPopularity(double popularity) {
        this.popularity = popularity;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public double getAverageVote() {
        return averageVote;
    }

    public void setAverageVote(double averageVote) {
        this.averageVote = averageVote;
    }

    public long getVoteCount() {
        return voteCount;
    }

    public void setVoteCount(long voteCount) {
        this.voteCount = voteCount;
    }

    public String getBackdropPath() {
        return backdropPath;
    }

    public void setBackdropPath(String backdropPath) {
        this.backdropPath = backdropPath;
    }

    @Override
    public String toString() {
        return "[" + this.id + ", " + this.title + "]";
    }

    //CHECKSTYLE:OFF
    @Override
    @SuppressWarnings("PMD")
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        MoviePreferences moviePreferences = (MoviePreferences) o;

        if (id != moviePreferences.id) return false;
        if (Double.compare(moviePreferences.popularity, popularity) != 0) return false;
        if (Double.compare(moviePreferences.averageVote, averageVote) != 0) return false;
        if (voteCount != moviePreferences.voteCount) return false;
        if (originalTitle != null ? !originalTitle.equals(moviePreferences.originalTitle) : moviePreferences.originalTitle != null)
            return false;
        if (overview != null ? !overview.equals(moviePreferences.overview) : moviePreferences.overview != null) return false;
        if (releaseDate != null ? !releaseDate.equals(moviePreferences.releaseDate) : moviePreferences.releaseDate != null) return false;
        if (posterPath != null ? !posterPath.equals(moviePreferences.posterPath) : moviePreferences.posterPath != null) return false;
        if (title != null ? !title.equals(moviePreferences.title) : moviePreferences.title != null) return false;
        return backdropPath != null ? backdropPath.equals(moviePreferences.backdropPath) : moviePreferences.backdropPath == null;

    }

    @Override
    @SuppressWarnings("PMD")
    public int hashCode() {
        int result;
        long temp;
        result = (int) (id ^ (id >>> 32));
        result = 31 * result + (originalTitle != null ? originalTitle.hashCode() : 0);
        result = 31 * result + (overview != null ? overview.hashCode() : 0);
        result = 31 * result + (releaseDate != null ? releaseDate.hashCode() : 0);
        result = 31 * result + (posterPath != null ? posterPath.hashCode() : 0);
        temp = Double.doubleToLongBits(popularity);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        result = 31 * result + (title != null ? title.hashCode() : 0);
        temp = Double.doubleToLongBits(averageVote);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        result = 31 * result + (int) (voteCount ^ (voteCount >>> 32));
        result = 31 * result + (backdropPath != null ? backdropPath.hashCode() : 0);
        return result;
    }
    //CHECKSTYLE:ON

    public ContentValues toContentValues() {
        ContentValues values = new ContentValues();
        values.put(MoviesConnect.MovieEntry._ID, id);
        values.put(MoviesConnect.MovieEntry.COLUMN_ORIGINAL_TITLE, originalTitle);
        values.put(MoviesConnect.MovieEntry.COLUMN_OVERVIEW, overview);
        values.put(MoviesConnect.MovieEntry.COLUMN_RELEASE_DATE, releaseDate);
        values.put(MoviesConnect.MovieEntry.COLUMN_POSTER_PATH, posterPath);
        values.put(MoviesConnect.MovieEntry.COLUMN_POPULARITY, popularity);
        values.put(MoviesConnect.MovieEntry.COLUMN_TITLE, title);
        values.put(MoviesConnect.MovieEntry.COLUMN_AVERAGE_VOTE, averageVote);
        values.put(MoviesConnect.MovieEntry.COLUMN_VOTE_COUNT, voteCount);
        values.put(MoviesConnect.MovieEntry.COLUMN_BACKDROP_PATH, backdropPath);
        return values;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(this.id);
        dest.writeString(this.originalTitle);
        dest.writeString(this.overview);
        dest.writeString(this.releaseDate);
        dest.writeString(this.posterPath);
        dest.writeDouble(this.popularity);
        dest.writeString(this.title);
        dest.writeDouble(this.averageVote);
        dest.writeLong(this.voteCount);
        dest.writeString(this.backdropPath);
    }
}
